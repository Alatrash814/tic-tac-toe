import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.*
import javax.swing.*
import javax.swing.JOptionPane.YES_NO_OPTION
import javax.swing.JOptionPane.YES_OPTION

class TicTacToe : JFrame,ActionListener{
    private var LabelShower = 0
    private var Turn1 = 0
    private var Turn = ' '
    private var MainPanel = JPanel()
    private var Label = JLabel("Tic-Tac-Toe")
    private var SecondPanel = JPanel()
    private var Panel1 = JPanel()
    private var Panel2 = JPanel()
    private var Panel3 = JPanel()
    private var Panel4 = JPanel()
    private var Panel5 = JPanel()
    private var Panel6 = JPanel()
    private var Panel7 = JPanel()
    private var Panel8 = JPanel()
    private var Panel9 = JPanel()
    private var Panel10 = JPanel()
    private var Panel11 = JPanel()
    private var Panel12 = JPanel()
    private var Panel13 = JPanel()
    private var Panel14 = JPanel()
    private var Panel15 = JPanel()

    private var Image = ImageIcon("D://Kotlin//Tic-Tac-Toe//src//Empty.png")
    private var TicIcon = ImageIcon("D://Kotlin//Tic-Tac-Toe//src//Tic.png")
    private var TocIcon = ImageIcon("D://Kotlin//Tic-Tac-Toe//src//Toc.png")
    private var HoverTicIcon = ImageIcon ("D://Kotlin//Tic-Tac-Toe//src//Tic2.png")
    private var HoverTocIcon = ImageIcon ("D://Kotlin//Tic-Tac-Toe//src//Toc2.png")
    private var ResetIcon = ImageIcon ("D://Kotlin//Tic-Tac-Toe//src//Reset.png")
    private var ExitIcon = ImageIcon ("D://Kotlin//Tic-Tac-Toe//src//Exit.png")
    private var Button1 = JButton(Image)
    private var Button2 = JButton(Image)
    private var Button3 = JButton(Image)
    private var Label4 = JLabel("Player X : ")
    private var Label5 = JLabel("0")
    private var Button6 = JButton(Image)
    private var Button7 = JButton(Image)
    private var Button8 = JButton(Image)
    private var Label9 = JLabel("Player O : ")
    private var Label10 = JLabel("0")
    private var Button11 = JButton(Image)
    private var Button12 = JButton(Image)
    private var Button13 = JButton(Image)
    private var Button14 = JButton(ResetIcon)
    private var Button15 = JButton(ExitIcon)
    private var Options =  arrayOf('X','O')
    private var CanClick = arrayOf(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
    private var CurrentState = arrayOf(' ',' ',' ',' ',' ',' ',' ',' ',' ')


    constructor() : super ("Tic-Tac-Toe"){

        MainPanel.layout = BorderLayout()
        MainPanel.border = BorderFactory.createLoweredBevelBorder()
        MainPanel.border = BorderFactory.createLineBorder(Color.BLACK,2)

        Label.background = Color.GRAY
        Label.horizontalAlignment = (Component.CENTER_ALIGNMENT).toInt()
        Label.foreground = Color.RED
        Label.font = Font("Tahoma",1,20)

        var GL = GridLayout()
        GL.columns = 5
        GL.rows = 3
        GL.hgap = 2
        GL.vgap = 2
        SecondPanel.layout = GL
        SecondPanel.background = Color.WHITE

        Panel1.layout = BorderLayout()
        Panel2.layout = BorderLayout()
        Panel3.layout = BorderLayout()
        Panel4.layout = BorderLayout()
        Panel5.layout = BorderLayout()
        Panel6.layout = BorderLayout()
        Panel7.layout = BorderLayout()
        Panel8.layout = BorderLayout()
        Panel9.layout = BorderLayout()
        Panel10.layout = BorderLayout()
        Panel11.layout = BorderLayout()
        Panel12.layout = BorderLayout()
        Panel13.layout = BorderLayout()
        Panel14.layout = BorderLayout()
        Panel15.layout = BorderLayout()

        Panel1.add(Button1)
        Panel2.add(Button2)
        Panel3.add(Button3)
        Panel4.add(Label4)
        Panel5.add(Label5)
        Panel6.add(Button6)
        Panel7.add(Button7)
        Panel8.add(Button8)
        Panel9.add(Label9)
        Panel10.add(Label10)
        Panel11.add(Button11)
        Panel12.add(Button12)
        Panel13.add(Button13)
        Panel14.add(Button14)
        Panel15.add(Button15)

        SecondPanel.add(Panel1)
        SecondPanel.add(Panel2)
        SecondPanel.add(Panel3)
        SecondPanel.add(Panel4)
        SecondPanel.add(Panel5)
        SecondPanel.add(Panel6)
        SecondPanel.add(Panel7)
        SecondPanel.add(Panel8)
        SecondPanel.add(Panel9)
        SecondPanel.add(Panel10)
        SecondPanel.add(Panel11)
        SecondPanel.add(Panel12)
        SecondPanel.add(Panel13)
        SecondPanel.add(Panel14)
        SecondPanel.add(Panel15)

        add(MainPanel)
        MainPanel.add(Label,BorderLayout.NORTH)
        MainPanel.add(SecondPanel,BorderLayout.CENTER)

        Button1.addActionListener(this)
        Button2.addActionListener(this)
        Button3.addActionListener(this)
        Button6.addActionListener(this)
        Button7.addActionListener(this)
        Button8.addActionListener(this)
        Button11.addActionListener(this)
        Button12.addActionListener(this)
        Button13.addActionListener(this)
        Button14.addActionListener(this)
        Button15.addActionListener(this)

        setSize(650,600)
        setLocation(100,100)
        UIManager.put("Button.border",BorderFactory.createEmptyBorder())
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
        SwingUtilities.updateComponentTreeUI(this)
        isVisible = true

        Turn1= JOptionPane.showOptionDialog(this,"First to play : ","Tic-Tac-Toe", YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE,null,Options,Options[0].toInt())

        if (Turn1 == 0) {
            Turn = 'X'
                        }//if statement

        else {
            Turn = 'O'
            MovePerform()
             }//else statement

        ChangeHover(Turn)

        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

                                               }//constructor

    override fun actionPerformed(e: ActionEvent?) {

        if (e!!.source == Button15){
            var Exit : Any= JOptionPane.showConfirmDialog(this,"Do you want to exit ?","Warning ! " , YES_NO_OPTION,JOptionPane.WARNING_MESSAGE)
            if (Exit == YES_OPTION)
                dispose()
                                   }//if statement

        else if (e!!.source == Button1 && CanClick[1] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button1.icon = TicIcon

            else
                Button1.icon = TocIcon

            CurrentState[0] = Turn
            TurnChanger()
            CanClick[1] = 0
            ChangeHover(Turn)
            Winning2()
                                                            }//else if statement

        else if (e!!.source == Button2 && CanClick[2] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button2.icon = TicIcon

            else
                Button2.icon = TocIcon

            CurrentState[1] = Turn
            TurnChanger()
            CanClick[2] = 0
            ChangeHover(Turn)
            Winning2()
                                                           }//else if statement

        else if (e!!.source == Button3 && CanClick[3] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button3.icon = TicIcon

            else
                Button3.icon = TocIcon

            CurrentState[2] = Turn
            CanClick[3] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                           }//else if statement

        else if (e!!.source == Button6 && CanClick[6] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button6.icon = TicIcon

            else
                Button6.icon = TocIcon

            CurrentState[3] = Turn
            CanClick[6] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                           }//else if statement

        else if (e!!.source == Button7 && CanClick[7] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button7.icon = TicIcon

            else
                Button7.icon = TocIcon

            CurrentState[4] = Turn
            CanClick[7] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                           }//else if statement

        else if (e!!.source == Button8 && CanClick[8] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button8.icon = TicIcon

            else
                Button8.icon = TocIcon

            CurrentState [5] = Turn
            CanClick[8] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                            }//else if statement

        else if (e!!.source == Button11 && CanClick[11] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button11.icon = TicIcon

            else
                Button11.icon = TocIcon

            CurrentState[6] = Turn
            CanClick[11] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                              }//else if

        else if (e!!.source == Button12 && CanClick[12] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button12.icon = TicIcon

            else
                Button12.icon = TocIcon

            CurrentState[7] = Turn
            CanClick[12] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                             }//else if

        else if (e!!.source == Button13 && CanClick[13] == 1){
            ChangeHover(Turn)
            if (Turn == 'X')
                Button13.icon = TicIcon

            else
                Button13.icon = TocIcon

            CurrentState[8] = Turn
            CanClick[13] = 0
            TurnChanger()
            ChangeHover(Turn)
            Winning2()
                                                             }//else if

        else if (e!!.source == Button14) {

            Button1.isEnabled = true
            Button1.icon = Image
            Button2.isEnabled = true
            Button2.icon = Image
            Button3.isEnabled = true
            Button3.icon = Image
            Button6.isEnabled = true
            Button6.icon = Image
            Button7.isEnabled = true
            Button7.icon = Image
            Button8.isEnabled = true
            Button8.icon = Image
            Button11.isEnabled = true
            Button11.icon = Image
            Button12.isEnabled = true
            Button12.icon = Image
            Button13.isEnabled = true
            Button13.icon = Image

            LabelShower = 0

            for (i in 0..14)
                CanClick[i] = 1

            for (i in 0..8)
                CurrentState[i] = ' '

            Turn1 = JOptionPane.showOptionDialog(this, "First to play : ", "Tic-Tac-Toe", YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, Options, Options[0].toInt())

            if (Turn1 == 0) {
                Turn = 'X'
                            }//if statement
            else {
                Turn = 'O'
                MovePerform()
                 }//else statement

            ChangeHover(Turn)

                                         }//else if statement

                                                    }//actionPerformed

    fun ChangeHover(Turn : Char) : Unit {

        if (Turn == 'X'){

            if (CanClick[1] == 1)
                Button1.rolloverIcon = HoverTicIcon
            else
                Button1.rolloverIcon = null

            if (CanClick[2] == 1)
                Button2.rolloverIcon = HoverTicIcon
            else
                Button2.rolloverIcon = null

            if (CanClick[3] == 1)
                Button3.rolloverIcon = HoverTicIcon
            else
                Button3.rolloverIcon = null

            if (CanClick[6] == 1)
                Button6.rolloverIcon = HoverTicIcon
            else
                Button6.rolloverIcon = null

            if (CanClick[7] == 1)
                Button7.rolloverIcon = HoverTicIcon
            else
                Button7.rolloverIcon = null

            if (CanClick[8] == 1)
                Button8.rolloverIcon = HoverTicIcon
            else
                Button8.rolloverIcon = null

            if (CanClick[11] == 1)
                Button11.rolloverIcon = HoverTicIcon
            else
                Button11.rolloverIcon = null

            if (CanClick[12] == 1)
                Button12.rolloverIcon = HoverTicIcon
            else
                Button12.rolloverIcon = null

            if (CanClick[13] == 1)
                Button13.rolloverIcon = HoverTicIcon
            else
                Button13.rolloverIcon = null

                         }//if statement

        else {
            if (CanClick[1] == 1)
                Button1.rolloverIcon = HoverTocIcon
            else
                Button1.rolloverIcon = null

            if (CanClick[2] == 1)
                Button2.rolloverIcon = HoverTocIcon
            else
                Button2.rolloverIcon = null

            if (CanClick[3] == 1)
                Button3.rolloverIcon = HoverTocIcon
            else
                Button3.rolloverIcon = null

            if (CanClick[6] == 1)
                Button6.rolloverIcon = HoverTocIcon
            else
                Button6.rolloverIcon = null

            if (CanClick[7] == 1)
                Button7.rolloverIcon = HoverTocIcon
            else
                Button7.rolloverIcon = null

            if (CanClick[8] == 1)
                Button8.rolloverIcon = HoverTocIcon
            else
                Button8.rolloverIcon = null

            if (CanClick[11] == 1)
                Button11.rolloverIcon = HoverTocIcon
            else
                Button11.rolloverIcon = null

            if (CanClick[12] == 1)
                Button12.rolloverIcon = HoverTocIcon
            else
                Button12.rolloverIcon = null

            if (CanClick[13] == 1)
                Button13.rolloverIcon = HoverTocIcon
            else
                Button13.rolloverIcon = null

              }//else statement

                                         }//ChangeHover

    fun TurnChanger() : Unit {

        if (Turn == 'X') {
            Turn = 'O'
            MovePerform()
                         }//if statement

        else if (Turn == 'O'){
            Turn = 'X'
                              }

                             }//TurnChanger

    fun Winning () : Int {

        if (CurrentState[0] == 'X' && CurrentState[1] == 'X' && CurrentState[2] == 'X')
            return -10

        else if (CurrentState[3] == 'X' && CurrentState[4] == 'X' && CurrentState[5] == 'X')
            return -10

        else if (CurrentState[6] == 'X' && CurrentState[7] == 'X' && CurrentState[8] == 'X')
            return -10

        else if (CurrentState[0] == 'X' && CurrentState[3] == 'X' && CurrentState[6] == 'X')
            return -10

        else if (CurrentState[1] == 'X' && CurrentState[4] == 'X' && CurrentState[7] == 'X')
            return -10

        else if (CurrentState[2] == 'X' && CurrentState[5] == 'X' && CurrentState[8] == 'X')
            return -10

        else if (CurrentState[0] == 'X' && CurrentState[4] == 'X' && CurrentState[8] == 'X')
            return -10

        else if (CurrentState[2] == 'X' && CurrentState[4] == 'X' && CurrentState[6] == 'X')
            return -10

        else if (CurrentState[0] == 'O' && CurrentState[1] == 'O' && CurrentState[2] == 'O')
            return 10

        else if (CurrentState[3] == 'O' && CurrentState[4] == 'O' && CurrentState[5] == 'O')
            return 10

        else if (CurrentState[6] == 'O' && CurrentState[7] == 'O' && CurrentState[8] == 'O')
            return 10

        else if (CurrentState[0] == 'O' && CurrentState[3] == 'O' && CurrentState[6] == 'O')
            return 10

        else if (CurrentState[1] == 'O' && CurrentState[4] == 'O' && CurrentState[7] == 'O')
            return 10

        else if (CurrentState[2] == 'O' && CurrentState[5] == 'O' && CurrentState[8] == 'O')
            return 10

        else if (CurrentState[0] == 'O' && CurrentState[4] == 'O' && CurrentState[8] == 'O')
            return 10

        else if (CurrentState[2] == 'O' && CurrentState[4] == 'O' && CurrentState[6] == 'O')
            return 10


        var Counter = 0

        for (i in 0..8){
            if (CurrentState[i] == ' ')
                break
            ++Counter
                            }//for loop

        if (Counter == 9)
            return 0


        return -1

                          }//Winning

    fun Disable () : Unit {

        Button1.disabledIcon = Button1.icon
        Button1.isEnabled = false
        Button2.disabledIcon = Button2.icon
        Button2.isEnabled = false
        Button3.disabledIcon = Button3.icon
        Button3.isEnabled = false
        Button6.disabledIcon = Button6.icon
        Button6.isEnabled = false
        Button7.disabledIcon = Button7.icon
        Button7.isEnabled = false
        Button8.disabledIcon = Button8.icon
        Button8.isEnabled = false
        Button11.disabledIcon = Button11.icon
        Button11.isEnabled = false
        Button12.disabledIcon = Button12.icon
        Button12.isEnabled = false
        Button13.disabledIcon = Button13.icon
        Button13.isEnabled = false

                            }//Disable

    fun GetBestMove (Turn2 : Char) : Pair<Int,Int>{

        var RV = Winning()
        if (RV == -10)
            return Pair(-10,0)

        else if (RV == 10)
            return Pair(10,0)

        else if (RV == 0)
           return Pair (0,0)

        var Moves = Vector<Pair<Int,Int>>()


        for (i in 0..8){

            if (CurrentState[i] == ' '){
                CurrentState[i] = Turn2
                var Poition = i
                var Score = 0

                if (Turn2 == 'O')
                    Score = GetBestMove('X').first

                else
                    Score = GetBestMove('O').first

                var X = Pair (Score,Poition)
                Moves.addElement(X)
                CurrentState[i] = ' '
                                        }//if statement

                             }//for loop

        var BestMove = 0

        if (Turn2 == 'O'){
            var BestScore = Int.MIN_VALUE
            for (i in 0 until Moves.size){
                if (Moves[i].first > BestScore){
                    BestMove = i
                    BestScore = Moves[i].first
                                               }//if statement
                                              }//for loop
                          }//if statement

        else if (Turn2 == 'X') {
            var BestScore = Int.MAX_VALUE
            for (i in 0 until Moves.size){
                if (Moves[i].first < BestScore){
                    BestMove = i
                    BestScore = Moves[i].first
                                                }//if statement
                                               }//for loop
                                 }//else statement

        return Moves[BestMove]
                                                    }//GetBestMove

    fun MovePerform () : Unit {

        var X = GetBestMove(Turn)

        if  (X.second == 0)
            Button1.doClick()
        else if (X.second == 1)
            Button2.doClick()
        else if (X.second == 2)
            Button3.doClick()
        else if (X.second == 3)
            Button6.doClick()
        else if (X.second == 4)
            Button7.doClick()
        else if (X.second == 5)
            Button8.doClick()
        else if (X.second == 6)
            Button11.doClick()
        else if (X.second == 7)
            Button12.doClick()
        else if (X.second == 8)
            Button13.doClick()
                               }//Move perform

    fun Winning2 () : Unit {

        var Chick = 0

        if (LabelShower != 1) {

            if (Button1.icon == TicIcon && Button2.icon == TicIcon && Button3.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                }//if statement

            else if (Button6.icon == TicIcon && Button7.icon == TicIcon && Button8.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                     }//else if statement

            else if (Button11.icon == TicIcon && Button12.icon == TicIcon && Button13.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                        }//else if statement

            else if (Button1.icon == TicIcon && Button6.icon == TicIcon && Button11.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button2.icon == TicIcon && Button7.icon == TicIcon && Button12.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button3.icon == TicIcon && Button8.icon == TicIcon && Button13.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button1.icon == TicIcon && Button7.icon == TicIcon && Button13.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button3.icon == TicIcon && Button7.icon == TicIcon && Button11.icon == TicIcon) {
                JOptionPane.showMessageDialog(this, "Player X Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label5.text = (Label5.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                        }//else if statement

            else if (Button1.icon == TocIcon && Button2.icon == TocIcon && Button3.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                     }//else if statement

            else if (Button6.icon == TocIcon && Button7.icon == TocIcon && Button8.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                     }//else if statement

            else if (Button11.icon == TocIcon && Button12.icon == TocIcon && Button13.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                       }//else if statement

            else if (Button1.icon == TocIcon && Button6.icon == TocIcon && Button11.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button2.icon == TocIcon && Button7.icon == TocIcon && Button12.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button3.icon == TocIcon && Button8.icon == TocIcon && Button13.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button1.icon == TocIcon && Button7.icon == TocIcon && Button13.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

            else if (Button3.icon == TocIcon && Button7.icon == TocIcon && Button11.icon == TocIcon) {
                JOptionPane.showMessageDialog(this, "Player O Wins !", "Tic-Tac-Toe", JOptionPane.INFORMATION_MESSAGE)
                Label10.text = (Label10.text.toInt() + 1).toString()
                Disable()
                Chick = 1
                LabelShower = 1
                                                                                                      }//else if statement

                               }

        var Counter = 0
        for (i in 0..8){
            if (CurrentState[i] == ' ')
                break
            ++Counter
                            }//for loop

        if (Counter == 9 && Chick == 0 && LabelShower != 1){
            JOptionPane.showMessageDialog(this,"Draw !","Tic-Tac-Toe",JOptionPane.INFORMATION_MESSAGE)
            Disable()
            LabelShower = 1
                                                            }//if statement

                             }//Winning2

                                          }//TicTacToe

fun main (args : Array<String>){
    TicTacToe()
                                }//main